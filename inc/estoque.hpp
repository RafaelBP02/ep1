#ifndef ESTOQUE
#define ESTOQUE

#include <vector>
#include <string>

#include "produtos.hpp"

class estoque : public produtos
{

    public:
        estoque();
        ~estoque();

        string salvaCategorias();

};

#endif