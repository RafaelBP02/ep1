#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <iostream>
#include <string>
#include "salvaDados.hpp"
using namespace std;

class Cliente : public salvaDados
{
  private:
    string nome;
    string cpf;
    string telefone;


  public:
    Cliente();
    Cliente(string nome, string cpf, string telefone);
    ~Cliente();

    void set_nome(string nome);
    string get_nome();
    void set_cpf(string cpf);
    string get_cpf();
    void set_telefone(string telefone);
    string get_telefone();

    void escreveDados();


};

#endif
