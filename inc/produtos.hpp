#ifndef PRODUTOS_HPP
#define PRODUTOS_HPP

#include <string>
#include "salvaDados.hpp"
using namespace std;

class produtos : public salvaDados
{
  private:
    int id;
    string categoriaAlimento;
    string nomeAlimento;
    int quantidade;
    float precoAlimento;

  public:
    produtos();
    produtos(int id, string categoriaAlimento, string nomeAlimento, int quantidade, float precoAlimento);
    ~produtos();

    void set_id(int id);
    int get_id();

    void set_nomeAlimento(string nomeAlimento);
    string get_nomeAlimento();

    void set_categoriaAlimento(string categoriaAlimento);
    string get_categoriaAlimento();

    void set_quantidade(int quantidade);
    int get_quantidade();


    void set_precoAlimento(float precoAlimento);
    float get_precoAlimento();
    

    void escreveDados();

    void criar_estoque(string categoriaAlimento, string nomeAlimento, int quantidade, float precoAlimento);


};

#endif
