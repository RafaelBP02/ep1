#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstring>

#include "estoque.hpp"
#include "salvaDados.hpp"

using namespace std;

template <typename T1>

T1 getInput()
{
    while (true)
    {
        T1 numero;
        cin >> numero;
        if(cin.fail())
        {
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada INVALIDA! Tente NOVAMENTE: " << endl;
        }
        else
        {
            cin.ignore(32767,'\n');
            return numero;
        }
    }
    
}

estoque::estoque()
{
    system("clear");
    cout << "\t---- MODO ESTOQUE ----\n" << endl;

    string cat_Produto = salvaCategorias();

    cout << "\n\nDigite o nome do produto\n";
    
    cout << "NOME: ";
    string nomeAlimento_in;
    getline(cin >> ws, nomeAlimento_in);

    cout << "Quantidade: ";
    int quantidade_in = getInput<int>();

    cout << "Quanto Custa?: ";
    float precoAlimento_in = getInput<float>();

    cout << "\n\n";

    produtos::criar_estoque(cat_Produto, nomeAlimento_in, quantidade_in, precoAlimento_in);

    //sytem("clear")

    cout << "Produto adicionado ao estoque com sucesso\n";
    cout << "Deseja continuar a operação? (1) Continuar / (2) Terminar: " << endl;

    int escolha = getInput<int>();
    if (escolha ==  1)
    {
        estoque();
    }
    else if(escolha == 2)
    {

    }
    else
    {
        do
        {
            cout << "\nERRO! Insira os números 1 ou 2: ";
            escolha = getInput<int>();
        } while (escolha <1 || escolha > 2);
        
    }
    
}

estoque::~estoque()
{

}

string estoque::salvaCategorias()
{
    vector <string> categoriaAlimento;

    string nomeAlimento_cat;
    fstream cat;
    cat.open("./doc/Produtos.txt/");
    if(cat.is_open())
    {
        while(cat >> nomeAlimento_cat)
        {
            categoriaAlimento.push_back(nomeAlimento_cat);
        }
        cat.close();
    }

    cout << "\n--- Listagem de categorias CRIADAS ---\n";

    for(unsigned int i = 0; i<categoriaAlimento.size(); i++)
    {
        cout << "->" << categoriaAlimento[i] << endl << endl;
    }

    cout << endl;
    cout << "Digite o nome da categoria desejada para utilização ou adicionar." << endl;
    cout << "Caso queira mais de uma categoria, SEPARE-AS por um ESPAÇO." << endl;
    cout << "Categorias: ";

    string entrada;
    getline(cin >> ws, entrada);

    for(unsigned int j = 0; j < entrada.length(); j++)
    {
        entrada[j] = toupper(entrada[j]);
    }

    return entrada;

}

