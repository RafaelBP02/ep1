#include "produtos.hpp"
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

produtos::produtos()
{
  cout << "pega produtos" << endl;
}

produtos::produtos(int id, string categoriaAlimento, string nomeAlimento, int quantidade, float precoAlimento)
{
  this->id = id;
  this->categoriaAlimento = categoriaAlimento;
  this->nomeAlimento = nomeAlimento;
  this->precoAlimento = precoAlimento;
  this->quantidade = quantidade;
}

void produtos::criar_estoque(string categoriaAlimento, string nomeAlimento, int quantidade, float precoAlimento)
{
  fstream contador_id;
  int id_contador;
  int id_final;
  contador_id.open("./doc/Produtos.txt");
  
  if(contador_id.is_open())
  {
    while(contador_id >> id_contador)
    {

    }
    id_final = id_contador + 1;
    ofstream atualizar_contador;
    atualizar_contador.open("./doc/Produtos.txt");
    if(atualizar_contador.is_open())
    {
      atualizar_contador << id_final;
    }
  }
}

produtos::~produtos()
{
  cout << "terminando o objeto" << '\n';
}

void produtos::set_id(int id)
{
  this->id = id;
}
int produtos::get_id()
{
  return id;
}

void produtos::set_categoriaAlimento(string categoriaAlimento)
{
  this->categoriaAlimento = categoriaAlimento;
}
string produtos::get_categoriaAlimento()
{
  return categoriaAlimento;
}

void produtos::set_nomeAlimento(string nomeAlimento)
{
  this->nomeAlimento = nomeAlimento;
}
string produtos::get_nomeAlimento()
{
  return nomeAlimento;
}

void produtos::set_precoAlimento(float precoAlimento)
{
  this->precoAlimento = precoAlimento;
}
float produtos::get_precoAlimento()
{
  return precoAlimento;
}

void produtos::set_quantidade(int quantidade)
{
  this->quantidade = quantidade;
}
int produtos::get_quantidade()
{
  return quantidade;
}

void produtos::escreveDados()
{
  ofstream produto;
  string end = "./doc/Produtos/";
  end.append(to_string(id));
  end.append(".txt");
  produto.open(end);

  if(produto.is_open())
  {
    produto << get_id() << endl;
    produto << get_categoriaAlimento() << endl;
    produto << get_nomeAlimento() << endl;
    produto << fixed << setprecision(2) << get_quantidade() << endl;
    produto << get_precoAlimento() << endl;
    produto.close();
  }
}